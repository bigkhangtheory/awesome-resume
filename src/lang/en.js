// English
const lang = {
    contact: 'Contact',
    born: 'Born',
    bornIn: 'in',
    experience: 'Experience',
    education: 'Education',
    skills: 'Skills',
    languages: 'Languages',
    certifications: 'Certifications',
    projects: 'Projects',
    about: 'About me'
};
export default lang;
